Pod::Spec.new do |s|
    s.name              = 'IMbeeCore'
    s.version           = '1.23.0'
    s.summary           = 'IMbee Core SDK'
    s.homepage          = 'https://docs.imbee.es/ios/imbee-core/'

    s.author            = { 'IMbee' => 'info@imbeemessenger.com' }
    s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :http => "https://gitlab.com/imbee/ios/specs-source/raw/main/IMbeeCore/1.23.0/IMbeeCore-1.23.0.zip" }

    s.pod_target_xcconfig = {
        'ENABLE_BITCODE' => 'YES',
        'SWIFT_VERSION' => '4.2'
    }

    s.ios.deployment_target = '9.0'
    s.ios.vendored_frameworks = 'IMbeeCore.framework'

    s.ios.dependency 'sqlite3', '~> 3.21'
    s.ios.dependency 'XMPPFramework', '~> 3.7.0imbee.4'
end
